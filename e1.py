'''EJERCICIO NÚMEROS PRIMOS'''
#__author__ = "Juan     Alvarez"
#__email_ = "juan.v.alvarez@unl.edu.ec"

import math


def es_primo(numero):
    """
    Función que determina si un numero es primo

    """

    if (numero <= 1):
        return False

    for i in range(2, math.ceil(math.sqrt(numero)) + 1):
        if (numero % i == 0 and i != numero):
            return False
    return True


while True:
    try:
        numero = int(input("inserta un numero / inserta (0) para terminar 🢂 "))
        if numero == 0:
            break
        if es_primo(numero):
            print("\nEl numero🢂 %s es primo" % numero)
        else:
            print("\nEl numero🢂 %s no es primo" % numero)
    except:
        print("\nValor no valido vuelva a ingresar el valor 🢂")